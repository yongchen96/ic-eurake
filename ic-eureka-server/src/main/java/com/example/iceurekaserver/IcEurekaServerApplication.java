package com.example.iceurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class IcEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IcEurekaServerApplication.class, args);
    }

}
